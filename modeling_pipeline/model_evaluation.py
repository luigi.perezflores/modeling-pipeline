import numpy as np
import pandas as pd

# KS table
def ks_table (df:pd.DataFrame, target:str, score:str, decile_column:str, window_name:str,model_id:str, nbins:int=10):
    """Create a KS table from a df with target and scores by deciles

    Args:
        df (pd.DataFrame): df with target, scores and decile_column 
        target (str): column with target variable
        score (str): column with score variable
        decile_column (str): column by which it is going to be grouped
        window_name (str): window_name
        model_id (str): unique model_id

    Returns:
        pd.DataFrame: KS table summary per decile

    Author:
        Jose Luis Perez Flores
    """
    df[decile_column] = pd.Categorical(df[decile_column], categories=range(nbins)[::-1], ordered=True)


    ks_table = pd.DataFrame()
    #Group by
    agg_dict = {
        score: ["count",'min',"max","sum","mean"],
        target: ["mean",'sum'],
    }
    
    ks_table = df.groupby(decile_column, observed=False).agg(agg_dict)
    ks_table.columns = ['_'.join(col).strip() for col in ks_table.columns.values]
    column_mapping = {
        'score_count': 'N',
        'score_min': 'min_score',
        'score_max': 'max_score',
        'score_sum': 'sum_score',
        'score_mean': 'avg_score',
        target + '_mean': 'avg_target',
        target + '_sum': 'target',
    }
    ks_table.rename(columns=column_mapping, inplace=True)

    #additional columns
    ks_table["no_target"] = ks_table['N'] - ks_table['target']

    ks_table["distr"] = ks_table['N'] / ks_table['N'].sum()
    ks_table['target_distr'] = ks_table['target'] / ks_table['target'].sum()

    # cumm values
    ks_table['cumm_target'] = ks_table['target'].cumsum() / ks_table['target'].sum()
    ks_table['cumm_no_target'] = ks_table['no_target'].cumsum() / ks_table['no_target'].sum()

    ks_table['KS'] = np.abs(ks_table['cumm_target'] - ks_table['cumm_no_target'])

    shifted_avg_target = ks_table["avg_target"].shift(-1)
    ks_table["ROB"] = np.where((ks_table["avg_target"].fillna(0) <= shifted_avg_target.fillna(1)) , 0, 1)

    ks_table['MAPE'] = np.where(ks_table['avg_target'] > 0,
                                (np.abs(ks_table['avg_target'] - ks_table['avg_score']) / ks_table['avg_target']), 0)
    ks_table['w_MAPE'] = ks_table['MAPE'] * ks_table['target_distr']

    ks_table["model_id"] = model_id
    ks_table["window"] = window_name
    ks_table.reset_index(inplace=True)


    return ks_table

# KS table for windows_dict
def run_model_decile_summary(windows_dict:dict, target:str, score:str, decile_column:str, model_id:str, nbins:int=10):
    """Runs KS table for all windows in windows_dict

    Args:
        windows_dict (dict): dictionary specifying which df belongs to each window. eg- {"df":df, "ITV":ITV} and others df
        target (str): column with target variable
        score (str): column with score variable
        decile_column (str): column by which it is going to be grouped
        model_id (str): unique model_id

    Returns:
        pd.DataFrame: KS table summary per model per decile

    Author:
        Jose Luis Perez Flores
    """
    ks_tables = []
    for window_name, df in windows_dict.items():
        ks_tbl = ks_table(df=df, 
                target=target, 
                score=score,
                decile_column=decile_column,
                window_name=window_name,
                model_id=model_id,
                nbins=nbins
                )
        ks_tables.append(ks_tbl)

    ks_table_conso = pd.concat(ks_tables, axis=0,ignore_index=True)
    return ks_table_conso

#  Model summary
def run_model_summary(df:pd.DataFrame, dev_key:str="df"):
    """Reduce model-decile summary into model summary

    Args:
        df (pd.DataFrame): run_model_decile_summary output.

    Returns:
        pd.DataFrame: KS table summary per model

    Author:
        Jose Luis Perez Flores
    """
    dev_key=dev_key.lower()
    windows = pd.Series(df['window'].unique()).str.lower().tolist()  

    df["MAPE_dec_0"]=np.where(df["decile"]==0, df["MAPE"],0)

    df["score_dec_min"]=np.where(df["decile"]==df["decile"].min(), df["avg_score"],0)
    df["score_dec_max"]=np.where(df["decile"]==df["decile"].max(), df["avg_score"],0)

    df.rename(columns={"sum_score":"score"},inplace=True)
    model_summary = df.pivot_table(
        index="model_id",
        columns="window",
        aggfunc={
        "N":"sum",
        "score":"sum",
        "target":"sum",
        "KS":"max",
        "ROB":"sum",
        "w_MAPE": "sum",
        "MAPE_dec_0":"sum",
        "score_dec_min":"sum",
        "score_dec_max":"sum"
        }
    ).reset_index()

    desired_order = ["model_id", "N", "ROB", "KS", "score", "target", "w_MAPE", "MAPE_dec_0","score_dec_min","score_dec_max"]
    model_summary = model_summary[desired_order]
    model_summary.columns = [f"{col[0]}_{str(col[1]).lower()}" for col in model_summary.columns]
    model_summary.rename(columns={"model_id_":"model_id"},inplace=True)

    # ADD KPIS
    for window in windows:
        model_summary[f"score_{window}"] = model_summary[f"score_{window}"]/model_summary[f"N_{window}"]
        model_summary[f"target_{window}"] = model_summary[f"target_{window}"]/model_summary[f"N_{window}"]
        if window != dev_key:
            model_summary[f"KS_drop_{window}"] = model_summary[f"KS_{window}"] - model_summary[f"KS_{dev_key}"]
        else:
            model_summary[f"score_slope_{window}"] = round(abs(model_summary[f"score_dec_min_{window}"] - model_summary[f"score_dec_max_{window}"]),2)
        model_summary.drop(columns=f"score_dec_min_{window}", inplace=True)
        model_summary.drop(columns=f"score_dec_max_{window}", inplace=True)


    return model_summary

# Plot_single_model_KS
def plot_single_model_all_ks(df:pd.DataFrame, window_names:list=["df","ITV","OTV"], invert_xaxis:bool=False):
    """Plots cumm distr of Target and no target. Needs ["window","decile","cumm_target","cumm_no_target","KS", "w_MAPE"]

    Args:
        df (pd.DataFrame): run_model_decile_summary output.
        window_names (list, optional): window to plot from df. Defaults to ["df","ITV","OTV"].

    Author:
        Jose Luis Perez Flores
    """
    import matplotlib.pyplot as plt
    from matplotlib.ticker import MultipleLocator

    fig, axs = plt.subplots(nrows=1, ncols=len(window_names), figsize=(18, 5))
    plot_i = 0
    for window_name in window_names:
        selected_ks_table = df[df["window"]==window_name].copy()
        max_ks = selected_ks_table["KS"].max()
        sum_wmape = selected_ks_table["w_MAPE"].sum()

        # Plot
        axs[plot_i].plot(selected_ks_table['decile'], selected_ks_table['cumm_target'], label='Cumm Target')
        axs[plot_i].plot(selected_ks_table['decile'], selected_ks_table['cumm_no_target'], label='Cumm No-Target')

        # Add labels and legend
        axs[plot_i].set_title(f'{window_name} | KS {max_ks*100:.1f}% - MAPE {sum_wmape*100:.1f}%')
        axs[plot_i].set_xlabel('decile')
        axs[plot_i].set_ylabel('% Distr cumm')
        axs[plot_i].legend()
        axs[plot_i].set_ylim(0, 1.05) 
        axs[plot_i].xaxis.set_major_locator(MultipleLocator(1))

        if invert_xaxis:
            axs[plot_i].invert_xaxis()

        plot_i+=1
    return

# Plot_single_model_all_score_vs_target
def plot_single_model_all_score_vs_target(df:pd.DataFrame, window_names:list=["df","ITV","OTV"], invert_xaxis:bool=False):
    """Plots cumm distr of Target and no target. Needs ["window","decile","cumm_target","cumm_no_target","KS", "w_MAPE"]

    Args:
        df (pd.DataFrame): run_model_decile_summary output.
        window_names (list, optional): window to plot from df. Defaults to ["df","ITV","OTV"].

    Author:
        Jose Luis Perez Flores
    """
    import matplotlib.pyplot as plt
    from matplotlib.ticker import MultipleLocator
    fig, axs = plt.subplots(nrows=1, ncols=len(window_names), figsize=(18, 5))
    plot_i = 0
    for window_name in window_names:
        selected_table = df[df["window"]==window_name].copy()
        max_ks = selected_table["KS"].max()
        sum_wmape = selected_table["w_MAPE"].sum()

        # Plot
        axs[plot_i].plot(selected_table['decile'], selected_table['avg_score'], label='Score')
        axs[plot_i].plot(selected_table['decile'], selected_table['avg_target'], label='Target')

        # Add labels and legend
        axs[plot_i].set_title(f'{window_name} | MAPE {sum_wmape*100:.1f}%')
        axs[plot_i].set_xlabel('Decile')
        axs[plot_i].set_ylabel('% Average')
        axs[plot_i].legend()
        axs[plot_i].set_ylim(0, 1)
        axs[plot_i].xaxis.set_major_locator(MultipleLocator(1))
        axs[plot_i].set_yticks(np.arange(0, 1.05, 0.1))
        axs[plot_i].grid(True, linestyle='--', alpha=0.3)
        if invert_xaxis:
            axs[plot_i].invert_xaxis()
        plot_i+=1
    return

#Grid Search for Classifier
def grid_search_clf(windows_dict, target, model_features, model, param_grid, suff, n_bins=10, dev_key="DEV"):
    from sklearn.model_selection import ParameterGrid
    from modeling_pipeline.variable_reduction import fit_deciles, transform_deciles

    windows_dict_={}
    model_summary_conso=[]
    model_summary_decile_conso=[]
        
    i=0
    for i_model in ParameterGrid(param_grid):
        i+=1
        #FIT MODEL
        model.set_params(**i_model)

        # Fit model
        model.fit(windows_dict[dev_key][model_features], windows_dict[dev_key][target])

        # Predict model
        for window, df in windows_dict.items():
            # Make predictions
            dev_pred = model.predict_proba(df[model_features])[:,-1]
            dev_pred = pd.Series(dev_pred, name='score')
            
            # FINAL TABLE -with scores
            df_w_pred = pd.concat([df[["application_id"] + model_features + [target]], dev_pred],axis=1)

            # windos dict tmp
            windows_dict_[window]=df_w_pred

        # Fit deciles
        dev_bins = fit_deciles(df=windows_dict_[dev_key],
                            var='score',
                            n=n_bins)

        # Transform deciles
        for window_name, df in windows_dict_.items():
            windows_dict_[window_name] = transform_deciles(df=df,
                                var='score', 
                                bins=dev_bins,
                                n=n_bins)

        model_decile_summary = run_model_decile_summary(
            windows_dict=windows_dict_, 
            target=target, 
            score='score',
            decile_column="decile",
            model_id="ID_"+str(i)+"_"+ suff,
            nbins=n_bins)

        model_decile_summary["config"] = str(i_model)

        model_summary = run_model_summary(df=model_decile_summary, dev_key=dev_key)
        model_summary["config"] = str(i_model)

        model_summary_decile_conso.append(model_decile_summary)
        model_summary_conso.append(model_summary)
        
    grid_search_deciles = pd.concat(model_summary_decile_conso,axis=0, ignore_index=True)
    grid_search = pd.concat(model_summary_conso,axis=0, ignore_index=True)

    return grid_search, grid_search_deciles, windows_dict_

#CV Grid Search for Classifier
def grid_search_CV(df, target, model_features, model, param_grid, suff, n_bins=10, num_folds=5):
    from sklearn.model_selection import StratifiedKFold
    from sklearn.model_selection import ParameterGrid
    from sklearn.model_selection import ParameterGrid
    from modeling_pipeline.variable_reduction import fit_deciles, transform_deciles

    model_summary_conso=[]
    model_summary_decile_conso=[]
    
    X=df[model_features]
    y=df[target]
     
    i=0
    for i_model in ParameterGrid(param_grid):
        i+=1
        # Creating a stratified k-fold object
        stratified_kfold = StratifiedKFold(n_splits=num_folds, shuffle=True, random_state=42)

        # Performing stratified cross-validation
        for fold, (train_index, test_index) in enumerate(stratified_kfold.split(X, y)):

            #K fold Split
            X_train, X_test = X.iloc[train_index], X.iloc[test_index]
            y_train, y_test = y.iloc[train_index], y.iloc[test_index]

            dataframes = [X_train, X_test, y_train, y_test]
            # Resetting indices for all DataFrames in the list
            for df in dataframes:
                df.reset_index(drop=True, inplace=True)

            # Fitting the model on the training data
            model.set_params(**i_model)
            model.fit(X_train, y_train)

            # Making predictions on the test data
            y_pred_train = pd.DataFrame(model.predict_proba(X_train)[:,-1], columns=['score'])
            y_pred_test = pd.DataFrame(model.predict_proba(X_test)[:,-1], columns=['score'])

            #Concat into single df
            train_w_pred = pd.concat([X_train, y_train, y_pred_train],axis=1)
            test_w_pred = pd.concat([X_test, y_test, y_pred_test],axis=1)
            
            # Fit deciles
            dev_bins = fit_deciles(df=train_w_pred,
                                var='score',
                                n=n_bins)

            # Transform deciles
            transform_deciles(df=train_w_pred,
                                var='score', 
                                bins=dev_bins,
                                n=n_bins)

            transform_deciles(df=test_w_pred,
                                var='score', 
                                bins=dev_bins,
                                n=n_bins)
            windows_dict_={"DEV":train_w_pred,
                        "ITV":test_w_pred}

            model_decile_summary = run_model_decile_summary(
                windows_dict=windows_dict_, 
                target=target, 
                score='score',
                decile_column="decile",
                model_id="ID_"+str(i)+"_"+ suff,
                nbins=n_bins)
        
            model_decile_summary["config"] = str(i_model)

            model_summary = run_model_summary(df=model_decile_summary, dev_key="DEV")
            model_summary["config"] = str(i_model)
            model_summary["k_fold"] = fold
            model_decile_summary["k_fold"] = fold
            

            model_summary_decile_conso.append(model_decile_summary)
            model_summary_conso.append(model_summary)
        grid_search_deciles = pd.concat(model_summary_decile_conso,axis=0, ignore_index=True)
        grid_search = pd.concat(model_summary_conso,axis=0, ignore_index=True)
    return grid_search, grid_search_deciles
