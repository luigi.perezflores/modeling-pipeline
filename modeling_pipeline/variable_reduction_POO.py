import numpy as np
import pandas as pd
import statsmodels.api as sm
from sklearn.feature_selection import SelectKBest, f_classif
from sklearn.linear_model import LogisticRegression
from varclushi import VarClusHi
from sklearn.preprocessing import MinMaxScaler

class VariableReduction:
    def __init__(
            self,
            data_dict:dict,
            dev_key:str,
            id_col:str,
            features:list,
            target:str,
            **kwargs
            ):
        self.data_dict = data_dict.copy()
        self.dev_key = dev_key
        self.all_features = features
        self.id_col = id_col
        self.features = features
        self.target = target
        self.params = kwargs
        self.features_pipeline = {"Init":{"size":len(self.features)}}
        self.cols_to_keep = [col for col in self.data_dict[self.dev_key] if col not in self.features]

    def scale(self):
        min_max_scaler = MinMaxScaler()
        for window, df in self.data_dict.items():
            if window==self.dev_key:
                X_scaled = min_max_scaler.fit(df[self.features])
            X_scaled = min_max_scaler.transform(df[self.features])

            df_scaled = pd.DataFrame(X_scaled, columns=self.features)
            df_scaled = pd.concat([df[self.cols_to_keep], df_scaled], axis=1)
            self.data_dict[window] = df_scaled.copy()

        self.features_pipeline["Scale"]= {"size":len(self.features)}
        print('  - Scale finished')

        return
    
    def iv(self, threshold:float=.02):
        df_= self.data_dict[self.dev_key].copy()
        lst_var=[]
        lst_iv=[]

        dummy_variables = df_[self.features].nunique()[df_[self.features].nunique() <= 4].index.tolist()
        num_variables = df_[self.features].nunique()[df_[self.features].nunique() > 4].index.tolist()

        # IV for Numeric Variables
        for var in num_variables:
            # Create bins for each variable
            _, bins = pd.qcut(df_[var], q=10, retbins=True, duplicates='drop')

            # Create a new column with binned values
            df_[var + '_bin'] = pd.cut(df_[var], bins=bins, include_lowest=True)

            # Calculate WoE (Weight of Evidence) and IV
            grouped = df_.groupby(var + '_bin',observed=False)[self.target].agg(['count', 'sum'])
            grouped['non_event'] = grouped['count'] - grouped['sum']
            grouped['event_rate'] = grouped['sum'] / grouped['sum'].sum()
            grouped['non_event_rate'] = grouped['non_event'] / grouped['non_event'].sum()
            grouped['woe'] = np.log(grouped['event_rate'] / grouped['non_event_rate'])
            grouped['iv'] = (grouped['event_rate'] - grouped['non_event_rate']) * grouped['woe'].replace({np.inf: 0, -np.inf: 0})

            # Sum IV for each variable
            iv = grouped['iv'].sum()

            # Append the results to the IV DataFrame
            lst_var.append(var)
            lst_iv.append(iv)

        # IV for Categorical Variables
        for var in dummy_variables:
            grouped = pd.crosstab(df_[var], df_[self.target])
            non_event_percent = grouped[0] / grouped[0].sum()
            event_percent = grouped[1] / grouped[1].sum()
            grouped["woe"] = np.log(event_percent / non_event_percent)
            grouped["iv"] = (event_percent - non_event_percent) * grouped["woe"].replace({np.inf: 0, -np.inf: 0})

            # Sum IV for each variable
            iv = grouped['iv'].sum()

            # Append the results to the IV DataFrame
            lst_var.append(var)
            lst_iv.append(iv)


        iv_table = pd.DataFrame({'variable':lst_var, 'iv':lst_iv})
        iv_table = iv_table.sort_values(by="iv",ascending=False).reset_index(drop=True)
        features = iv_table[iv_table["iv"]>threshold]["variable"].to_list()

        self.features_pipeline["IV"]= {"size":len(features),"dropped":len(self.features)- len(features)}
        self.features = features
        self.iv_table = iv_table

        for window, df in self.data_dict.items():
            self.data_dict[window] = df[self.cols_to_keep + self.features]

        print('  - IV finished')

        return

    def fit_deciles(self, df:pd.DataFrame, var:str, n:int=10):
        _, bins = pd.qcut(df[var], q=n, retbins=True,duplicates="drop")
        return bins

    def transform_deciles(self, df:pd.DataFrame, var:str, bins:int,n:int=10):
        tmp_dec = pd.cut(df[var], bins=bins, labels=False, include_lowest=True)
        df['decile'] = n - tmp_dec - 1
        return df

    def csi(self, n_bins:int=10, threshold:float=.10):
        dev_ = self.data_dict[self.dev_key].copy()
        csi_deciles =[]
        
        for var in self.features:
            for window, df_test in self.data_dict.items():
                if window==self.dev_key:
                    bins = self.fit_deciles(df=dev_, var=var, n=n_bins)
                    dev_by_decile = self.transform_deciles(df=dev_,var=var,bins=bins,n=n_bins)

                    csi_table_dev = dev_by_decile.groupby("decile").agg({"decile":"count"})
                    csi_table_dev.columns=["distr_dev_"]
                    csi_table_dev["distr_dev_"]=csi_table_dev["distr_dev_"]/csi_table_dev["distr_dev_"].sum()
                    csi_table_dev
                else:
                    itv_ = df_test.copy()
                    itv_by_decile = self.transform_deciles(df=itv_,var=var,bins=bins,n=n_bins)

                    csi_table_test = itv_by_decile.groupby("decile").agg({"decile":"count"})
                    csi_table_test.columns=["distr_test_"]
                    csi_table_test["distr_test_"]=csi_table_test["distr_test_"]/csi_table_test["distr_test_"].sum()
                    csi_table_test

                    csi_table = pd.concat([csi_table_dev ,csi_table_test],axis=1)
                    csi_table["variable"]=var
                    csi_table["diff"] = csi_table["distr_dev_"] - csi_table["distr_test_"]
                    csi_table["ln_dev_/test_"] = np.log(csi_table["distr_dev_"]/csi_table[f"distr_test_"])
                    csi_table[f"csi_{window}"] = csi_table["diff"] * csi_table[f"ln_dev_/test_"]
                    csi_deciles.append(csi_table)

        csi_deciles = pd.concat(csi_deciles)
        csi_deciles = csi_deciles.reset_index(drop=False)
        csi_cols = [col for col in csi_deciles.columns if col.startswith("csi")]
        csi_table = csi_deciles.groupby("variable")[csi_cols].sum().reset_index(drop=False)

        drop_var_csi=set()
        for col in csi_cols:
            feat = csi_table[csi_table[col]<threshold]["variable"].to_list()
            drop_var_csi.union(feat)
        drop_var_csi = list(drop_var_csi)
        features = [item for item in self.features if item not in drop_var_csi]

        self.features_pipeline["CSI"]= {"size":len(features),"dropped":len(self.features)- len(features)}
        self.csi_table = csi_table
        self.features = features

        for window, df in self.data_dict.items():
            self.data_dict[window] = df[self.cols_to_keep + self.features]

        print('  - CSI finished')

        return

    def high_correlation(self, threshold:float=.95):
        df= self.data_dict[self.dev_key].copy()
        threshold=.95

        #Corr Matrix & Upper Triangle
        corr_matrix = df[self.features].corr().abs()
        upper_tri = corr_matrix.where(np.triu(np.ones(corr_matrix.shape),k=1).astype(bool))

        #List of correalated vars
        highly_corr = upper_tri>threshold
        rows= highly_corr.any(axis=1)
        cols= highly_corr.any(axis=0)
        col_list = cols[cols==True].index.to_list()

        #Load IV
        iv_table = self.iv_table

        #Drop correlated vars with IV criterion
        corr_table = upper_tri[rows][col_list]
        corr_table.index.name="variable1"
        corr_table = corr_table.reset_index() 

        corr_table = corr_table.melt(id_vars=["variable1"], var_name="variable2", value_name="corr")
        corr_table = corr_table[corr_table["corr"]>threshold]
        corr_table = corr_table.merge(iv_table, left_on="variable1", right_on="variable").drop(columns="variable").rename(columns={"iv":"iv1"})
        corr_table = corr_table.merge(iv_table, left_on="variable2", right_on="variable").drop(columns="variable").rename(columns={"iv":"iv2"})
        corr_table.loc[corr_table["iv1"]<corr_table["iv2"],"drop"] = corr_table["variable1"]
        corr_table.loc[corr_table["iv1"]>=corr_table["iv2"],"drop"] = corr_table["variable2"]

        drop_var_corr = corr_table["drop"].unique().tolist()
        features = [item for item in self.features if item not in drop_var_corr]

        self.features_pipeline["Corr"]= {"size":len(features),"dropped":len(self.features)- len(features)}
        self.features = features
        self.corr_table = corr_table

        for window, df in self.data_dict.items():
            self.data_dict[window] = df[self.cols_to_keep + self.features]

        print('  - Correlation finished')

        return 

    def ttest(self, threshold:float=.05):
        df= self.data_dict[self.dev_key].copy()
        k_best = SelectKBest(score_func=f_classif, k='all')
        k_best.fit(df[self.features], df[self.target])
        ttest_table = pd.DataFrame({'variable': self.features, 'pval': k_best.pvalues_}).sort_values(by="pval").reset_index(drop=True)

        drop_var_ttest = ttest_table[ttest_table["pval"]>=threshold]['variable'].to_list()
        features = [item for item in self.features if item not in drop_var_ttest]

        self.features_pipeline["Ttest"] = {"size":len(features),"dropped":len(self.features)- len(features)}
        self.features = features
        self.corr_table = ttest_table

        for window, df in self.data_dict.items():
            self.data_dict[window] = df[self.cols_to_keep + self.features]

        print('  - T-Test finished')

        return

    def cnf(self, lower_percentile:int=1, upper_percentile:int=99):
        df = self.data_dict[self.dev_key].copy()
        cnf_limits_dict = {}
        #Fit CnF
        for column in self.features:
            lower_limit = np.percentile(df[column], lower_percentile)
            upper_limit = np.percentile(df[column], upper_percentile)
            cnf_limits_dict[column] = (lower_limit, upper_limit)

        #Transform CnF
        for _, df_ in self.data_dict.items():
            for column, (lower_limit, upper_limit) in cnf_limits_dict.items():
                df_[column] = np.clip(df[column], lower_limit, upper_limit)
            df_.reset_index(drop=True)

        cnf_limits_table = pd.DataFrame(cnf_limits_dict).T
        cnf_limits_table.columns=["lb","ub"]

        self.cnf_limits_table = cnf_limits_table
        self.features_pipeline["CnF"]= {"size":len(self.features)}
        print('  - Cap & Floor finished')

        return

    def magic_loop(windows_dict:dict, features:list, target:str, threshold_importance:float, min_features:int, dev_key:str="DEV"):
        from sklearn.ensemble import GradientBoostingClassifier
        from modeling_pipeline.model_evaluation import run_model_decile_summary, run_model_summary
        model_features = features.copy()

        model_summary_conso = []
        while len(model_features)>=min_features:

            windows_dict_tmp={}

            dev_x = windows_dict[dev_key][model_features].copy()
            dev_y = windows_dict[dev_key][target].copy()

            gbm_model = GradientBoostingClassifier(n_estimators=100, 
            #FIT MODEL
                                                subsample=0.8,
                                                learning_rate=0.01, 
                                                max_depth=4,
                                                min_samples_leaf=0.1, 
                                                random_state=42
                                                )
            # Train the model
            gbm_model.fit(dev_x, dev_y)
            
            for window, df in windows_dict.items():
                df_x = df[model_features].copy()
                df_y = df[target].copy()

                # Make predictios
                df_pred = gbm_model.predict_proba(df_x)[:,1]
                df_pred = pd.Series(df_pred, name="score")

                # FINAL TABLE -with scores
                df_w_pred = pd.concat([df_y, df_pred],axis=1)

                # windos dict tmp
                windows_dict_tmp[window]=df_w_pred

            # Fit deciles
            dev_bins = fit_deciles(df=windows_dict_tmp[dev_key],
                                var="score",
                                n=10)
            # Transform deciles
            for window, df in windows_dict_tmp.items():
                df = transform_deciles(df=df,
                                    var="score", 
                                    bins=dev_bins,
                                    n=10)
                
            ## Run ALL windows per decile
            model_decile_summary = run_model_decile_summary(
                windows_dict=windows_dict_tmp, 
                target=target, 
                score="score",
                decile_column="decile",
                model_id="GBM_feat_"+str(len(gbm_model.feature_names_in_)))

            model_summary = run_model_summary(df=model_decile_summary, dev_key=dev_key)
            model_summary["features"] = str(model_features)
            model_summary_conso.append(model_summary)

            feature_importances = gbm_model.feature_importances_

            sorted_features = sorted(zip(model_features, feature_importances), key=lambda x: x[1], reverse=True)
            model_features, importance_values = zip(*sorted_features)

            # Calculating cumulative sum
            cumulative_sum = np.cumsum(importance_values)

            # Creating a new list of tuples with feature name and cumulative importance
            cumulative_feature_importances = list(zip(model_features, cumulative_sum))

            # Filter features based on cumulative importance
            model_features = [feature for feature, cum_importance in cumulative_feature_importances if cum_importance <= threshold_importance]

        model_summary_conso = pd.concat(model_summary_conso,axis=0,ignore_index=True)
        return model_summary_conso

    def backward(self, criterio:str="aic" ,min_var:int=7):
        df = self.data_dict[self.dev_key]
        X = df[self.features]
        y = df[self.target]
        drop_var_backward = []
        best = False

        while len(X.columns)>min_var and best==False:
            initial_model = sm.Logit(y,sm.add_constant(X)).fit(disp=False) 
            iv_initial = initial_model.aic if criterio == 'aic' else initial_model.bic
            print(f'BIC/AIC: {iv_initial}','\t  | N vars:',len(X.columns))
            dic = {}
            for variable in X.columns:
                X_drop = X.drop(columns=variable).copy()
                model = sm.Logit(y,sm.add_constant(X_drop)).fit(disp=False)
                ic = model.aic if criterio == 'aic' else model.bic
                dic[variable]= ic
            min_ic = min(dic.values())         
            if min_ic >= iv_initial:
                best = True
            else:    
                min_ic_var = min(dic,key=dic.get)
                X = X.drop(columns=min_ic_var).copy()
                drop_var_backward.append(min_ic_var)      
        features = list(set(self.features)-set(drop_var_backward))
        best_model = sm.Logit(y,sm.add_constant(df[features])).fit(disp=False)      
        backward_table = best_model.summary()

        self.features_pipeline["Backward"] = {"size":len(features),"dropped":len(self.features)- len(features)}
        self.features = features
        self.backward_table = backward_table

        for window, df in self.data_dict.items():
            self.data_dict[window] = df[self.cols_to_keep + self.features]

        print('  - Backward finished')

        return

    def lasso(self):
        df= self.data_dict[self.dev_key].copy()

        clf_lr = LogisticRegression(random_state=42, 
                                penalty="l1",
                                solver='liblinear',
                                max_iter=100000)
        clf_lr.fit(df[self.features], df[self.target])

        lasso_table = zip(self.features, clf_lr.coef_.flatten())
        lasso_table = pd.DataFrame(lasso_table,columns=["var","coef"]).sort_values("coef").reset_index(drop=True)
        features = [feature for feature, value in zip(self.features, clf_lr.coef_.flatten()) if value != 0]

        self.features_pipeline["Lasso"]= {"size":len(features),"dropped":len(self.features)- len(features)}
        self.features = features
        self.lasso_table = lasso_table

        for window, df in self.data_dict.items():
            self.data_dict[window] = df[self.cols_to_keep + self.features]

        print('  - Lasso finished')

        return

    def varclus(self):
        df= self.data_dict[self.dev_key].copy()
        
        demo1_vc = VarClusHi(df[self.features],
                            maxeigval2=1,
                            maxclus=None)
        demo1_vc.varclus()
        varclus_table = demo1_vc.info

        rs = demo1_vc.rsquare
        min_ratio_idx = rs.groupby('Cluster')['RS_Ratio'].idxmin()
        min_ratio_idx
        representante_cluster = rs.loc[min_ratio_idx]
        features = representante_cluster['Variable'].to_list()

        self.features_pipeline["VarClus"]= {"size":len(features),"dropped":len(self.features)- len(features)}
        self.features = features
        self.varclus_table = varclus_table

        for window, df in self.data_dict.items():
            self.data_dict[window] = df[self.cols_to_keep + self.features]

        print('  - VarClus finished')

        return
    
    def add_method(self, method_name, **kwargs):
        self.methods.append((method_name, kwargs))

    def add_methods(self, *method_args):
        self.methods=[]
        for method_arg in method_args:
            if isinstance(method_arg, tuple):
                method_name, kwargs = method_arg[0], method_arg[1]
            else:
                method_name, kwargs = method_arg, {}
            self.add_method(method_name, **kwargs)

    def run_pipeline(self):
        for method_name, kwargs in self.methods:
            method_to_call = getattr(self, method_name)
            method_to_call(**kwargs)
        return self.features_pipeline