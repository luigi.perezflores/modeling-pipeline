import numpy as np
import pandas as pd
import statsmodels.api as sm
from sklearn.feature_selection import SelectKBest, f_classif
from sklearn.linear_model import LogisticRegression
from varclushi import VarClusHi
import time

def iv(df:pd.DataFrame,features_list:list,target:str, threshold:float=.02):
    df_=df.copy()
    lst_var=[]
    lst_iv=[]

    dummy_variables = df[features_list].nunique()[df[features_list].nunique() <= 4].index.tolist()
    num_variables = df[features_list].nunique()[df[features_list].nunique() > 4].index.tolist()

    # IV for Numeric Variables
    for var in num_variables:
        # Create bins for each variable
        _, bins = pd.qcut(df_[var], q=10, retbins=True, duplicates='drop')

        # Create a new column with binned values
        df_[var + '_bin'] = pd.cut(df_[var], bins=bins, include_lowest=True)

        # Calculate WoE (Weight of Evidence) and IV
        grouped = df_.groupby(var + '_bin',observed=False)[target].agg(['count', 'sum'])
        grouped['non_event'] = grouped['count'] - grouped['sum']
        grouped['event_rate'] = grouped['sum'] / grouped['sum'].sum()
        grouped['non_event_rate'] = grouped['non_event'] / grouped['non_event'].sum()
        grouped['woe'] = np.log(grouped['event_rate'] / grouped['non_event_rate'])
        grouped['iv'] = (grouped['event_rate'] - grouped['non_event_rate']) * grouped['woe'].replace({np.inf: 0, -np.inf: 0})

        # Sum IV for each variable
        iv = grouped['iv'].sum()

        # Append the results to the IV DataFrame
        lst_var.append(var)
        lst_iv.append(iv)

    # IV for Categorical Variables
    for var in dummy_variables:
        grouped = pd.crosstab(df[var], df[target])
        non_event_percent = grouped[0] / grouped[0].sum()
        event_percent = grouped[1] / grouped[1].sum()
        grouped["woe"] = np.log(event_percent / non_event_percent)
        grouped["iv"] = (event_percent - non_event_percent) * grouped["woe"].replace({np.inf: 0, -np.inf: 0})

        # Sum IV for each variable
        iv = grouped['iv'].sum()

        # Append the results to the IV DataFrame
        lst_var.append(var)
        lst_iv.append(iv)


    iv_table = pd.DataFrame({'variable':lst_var, 'iv':lst_iv})
    iv_table = iv_table.sort_values(by="iv",ascending=False).reset_index(drop=True)
    drop_var_iv = iv_table[iv_table["iv"]<=threshold]["variable"].to_list()
    return iv_table, drop_var_iv

def fit_deciles(df:pd.DataFrame,var:str,n:int=10):
    _, bins = pd.qcut(df[var], q=n, retbins=True,duplicates="drop")
    return bins

def transform_deciles(df:pd.DataFrame, var:str, bins:int,n:int=10):
    tmp_dec = pd.cut(df[var], bins=bins, labels=False, include_lowest=True)
    df['decile'] = n - tmp_dec - 1
    return df

def csi(df_fit:pd.DataFrame, df_test:pd.DataFrame, features_list:list, n_bins:int=10, threshold:float=.10):
    dev_ = df_fit.copy()
    itv_ = df_test.copy()
    csi_deciles =[]
    for var in features_list:
        bins = fit_deciles(df=dev_,var=var,n=n_bins)
        dev_by_decile = transform_deciles(df=dev_,var=var,bins=bins,n=n_bins)
        itv_by_decile = transform_deciles(df=itv_,var=var,bins=bins,n=n_bins)

        csi_table1 = dev_by_decile.groupby("decile").agg({"decile":"count"})
        csi_table1.columns=["distr_dev_"]
        csi_table1["distr_dev_"]=csi_table1["distr_dev_"]/csi_table1["distr_dev_"].sum()
        csi_table1

        csi_table2 = itv_by_decile.groupby("decile").agg({"decile":"count"})
        csi_table2.columns=["distr_itv_"]
        csi_table2["distr_itv_"]=csi_table2["distr_itv_"]/csi_table2["distr_itv_"].sum()
        csi_table2

        csi_table = pd.concat([csi_table1 ,csi_table2],axis=1)
        csi_table["diff"] = csi_table["distr_dev_"] - csi_table["distr_itv_"]
        csi_table["ln_dev_/itv_"] = np.log(csi_table["distr_dev_"]/csi_table["distr_itv_"])
        csi_table["csi"] = csi_table["diff"] * csi_table["ln_dev_/itv_"]
        csi_table["variable"]=var
        csi_deciles.append(csi_table)
    csi_deciles = pd.concat(csi_deciles)
    csi_deciles = csi_deciles.reset_index(drop=False)
    csi_table = csi_deciles.groupby("variable").agg({"csi":"sum"}).reset_index(drop=False)
    drop_var_csi = csi_table[csi_table["csi"]>threshold]["variable"].to_list()

    return csi_table, drop_var_csi

def high_correlation(df:pd.DataFrame, features:str, target:str, threshold:float):
    df=df.copy()
    threshold=.95

    #Corr Matrix & Upper Triangle
    corr_matrix = df[features].corr().abs()
    upper_tri = corr_matrix.where(np.triu(np.ones(corr_matrix.shape),k=1).astype(bool))

    #List of correalated vars
    highly_corr = upper_tri>threshold
    rows= highly_corr.any(axis=1)
    cols= highly_corr.any(axis=0)
    col_list = cols[cols==True].index.to_list()

    #Load IV
    iv_table, _ = iv(df=df,
                     features_list=features,
                     target=target)

    #Drop correlated vars with IV criterion
    corr_table = upper_tri[rows][col_list]
    corr_table.index.name="variable1"
    corr_table = corr_table.reset_index() 

    corr_table = corr_table.melt(id_vars=["variable1"], var_name="variable2", value_name="corr")
    corr_table = corr_table[corr_table["corr"]>threshold]
    corr_table = corr_table.merge(iv_table, left_on="variable1", right_on="variable").drop(columns="variable").rename(columns={"iv":"iv1"})
    corr_table = corr_table.merge(iv_table, left_on="variable2", right_on="variable").drop(columns="variable").rename(columns={"iv":"iv2"})
    corr_table.loc[corr_table["iv1"]<corr_table["iv2"],"drop"] = corr_table["variable1"]
    corr_table.loc[corr_table["iv1"]>=corr_table["iv2"],"drop"] = corr_table["variable2"]
    drop_var_corr = corr_table["drop"].unique().tolist()
    return corr_table, drop_var_corr

def ttest(df:pd.DataFrame, features:list, target:str, threshold:float=.05):
    k_best = SelectKBest(score_func=f_classif, k='all')
    k_best.fit(df[features], df[target])
    ttest_table = pd.DataFrame({'variable': features, 'pval': k_best.pvalues_}).sort_values(by="pval").reset_index(drop=True)
    drop_var_ttest = ttest_table[ttest_table["pval"]>=threshold]['variable'].to_list()
    return ttest_table, drop_var_ttest

def fit_cnf(df:pd.DataFrame, features:list, lower_percentile:int=1, upper_percentile:int=99):
    cnf_limits_dict = {}
    for column in features:
        # Calculate percentiles for each column
        lower_limit = np.percentile(df[column], lower_percentile)
        upper_limit = np.percentile(df[column], upper_percentile)
        cnf_limits_dict[column] = (lower_limit, upper_limit)
    return cnf_limits_dict

def transform_cnf(df:pd.DataFrame, cnf_limits_dict:dict):
    df_ = df.copy()
    for column, (lower_limit, upper_limit) in cnf_limits_dict.items():
        df_[column] = np.clip(df[column], lower_limit, upper_limit)
    return df_.reset_index(drop=True)

def magic_loop(windows_dict:dict, features:list, target:str, threshold_importance:float, min_features:int, dev_key:str="DEV"):
    from sklearn.ensemble import GradientBoostingClassifier
    from modeling_pipeline.model_evaluation import run_model_decile_summary, run_model_summary
    model_features = features.copy()

    model_summary_conso = []
    while len(model_features)>=min_features:

        windows_dict_tmp={}

        dev_x = windows_dict[dev_key][model_features].copy()
        dev_y = windows_dict[dev_key][target].copy()

        gbm_model = GradientBoostingClassifier(n_estimators=100, 
        #FIT MODEL
                                            subsample=0.8,
                                            learning_rate=0.01, 
                                            max_depth=4,
                                            min_samples_leaf=0.1, 
                                            random_state=42
                                            )
        # Train the model
        gbm_model.fit(dev_x, dev_y)
        
        for window, df in windows_dict.items():
            df_x = df[model_features].copy()
            df_y = df[target].copy()

            # Make predictios
            df_pred = gbm_model.predict_proba(df_x)[:,1]
            df_pred = pd.Series(df_pred, name="score")

            # FINAL TABLE -with scores
            df_w_pred = pd.concat([df_y, df_pred],axis=1)

            # windos dict tmp
            windows_dict_tmp[window]=df_w_pred

        # Fit deciles
        dev_bins = fit_deciles(df=windows_dict_tmp[dev_key],
                            var="score",
                            n=10)
        # Transform deciles
        for window, df in windows_dict_tmp.items():
            df = transform_deciles(df=df,
                                var="score", 
                                bins=dev_bins,
                                n=10)
            
        ## Run ALL windows per decile
        model_decile_summary = run_model_decile_summary(
            windows_dict=windows_dict_tmp, 
            target=target, 
            score="score",
            decile_column="decile",
            model_id="GBM_feat_"+str(len(gbm_model.feature_names_in_)))

        model_summary = run_model_summary(df=model_decile_summary, dev_key=dev_key)
        model_summary["features"] = str(model_features)
        model_summary_conso.append(model_summary)

        feature_importances = gbm_model.feature_importances_

        sorted_features = sorted(zip(model_features, feature_importances), key=lambda x: x[1], reverse=True)
        model_features, importance_values = zip(*sorted_features)

        # Calculating cumulative sum
        cumulative_sum = np.cumsum(importance_values)

        # Creating a new list of tuples with feature name and cumulative importance
        cumulative_feature_importances = list(zip(model_features, cumulative_sum))

        # Filter features based on cumulative importance
        model_features = [feature for feature, cum_importance in cumulative_feature_importances if cum_importance <= threshold_importance]

    model_summary_conso = pd.concat(model_summary_conso,axis=0,ignore_index=True)
    return model_summary_conso

def backwardlr(df:pd.DataFrame, features:list, target:str, criterio:str="aic" ,min_var:int=7):
    start_time = time.time()
    X = df[features]
    y = df[target]
    drop_var_backward = []
    best = False

    while len(X.columns)>min_var and best==False:
        initial_model = sm.Logit(y,sm.add_constant(X)).fit(disp=False) 
        iv_initial = initial_model.aic if criterio == 'aic' else initial_model.bic
        print(f'BIC/AIC: {iv_initial}','\t | N vars:',len(X.columns))
        dic = {}
        for variable in X.columns:
            X_drop = X.drop(columns=variable).copy()
            model = sm.Logit(y,sm.add_constant(X_drop)).fit(disp=False)
            ic = model.aic if criterio == 'aic' else model.bic
            dic[variable]= ic
        min_ic = min(dic.values())         
        if min_ic >= iv_initial:
            best = True
        else:    
            min_ic_var = min(dic,key=dic.get)
            X = X.drop(columns=min_ic_var).copy()
            drop_var_backward.append(min_ic_var)      
    elapsed_time = time.time() - start_time
    print(f"Iteration time: {elapsed_time:.2f} seconds")
    vars_to_keep = list(set(features)-set(drop_var_backward))
    best_model = sm.Logit(y,sm.add_constant(df[vars_to_keep])).fit(disp=False)      
    backward_table = best_model.summary()

    return backward_table, drop_var_backward

def lasso(df:pd.DataFrame, features:list,target:str,):
    clf_lr = LogisticRegression(random_state=42, 
                            penalty="l1",
                            solver='liblinear',
                            max_iter=100000)
    clf_lr.fit(df[features],
           df[target])

    lasso_table = zip(features, clf_lr.coef_.flatten())
    lasso_table = pd.DataFrame(lasso_table,columns=["var","coef"]).sort_values("coef").reset_index(drop=True)
    drop_var_lasso = [feature for feature, value in zip(features, clf_lr.coef_.flatten()) if value == 0]
    return lasso_table, drop_var_lasso

def varclus(df:pd.DataFrame, features:list):
    demo1_vc = VarClusHi(df[features],
                        maxeigval2=1,
                        maxclus=None)
    demo1_vc.varclus()
    demo1_vc.info

    rs = demo1_vc.rsquare
    min_ratio_idx = rs.groupby('Cluster')['RS_Ratio'].idxmin()
    min_ratio_idx
    representante_cluster = rs.loc[min_ratio_idx]
    varclus_features = representante_cluster['Variable'].to_list()
    return varclus_features