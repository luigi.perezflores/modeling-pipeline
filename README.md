# modeling-pipeline

Modeling Pipeline es para los procesos Variable Reduction & Model Evaluation

## 01 Descipción

En Variable Reduction encontrarás metodos como:
 - Cap and Floor (CnF)
 - Information Value (IV)
 - Characteristic Stability Index (CSI)
 - Weight of Evidence Transformation (WoE)
 - Correlación con IV
 - T test
 - VarClus
 - Magic Loop (GBM Iterativo)

En Model Evaluation encontrarás metodos como:
  - Analisis por bines
  - Grid Search por bines
  - CV Grid Search por bines


## 02 Notebooks

## 03 Auxiliar Input Files
  - data.csv  

## ¿Cómo correr?
Es una librería PIP instalable